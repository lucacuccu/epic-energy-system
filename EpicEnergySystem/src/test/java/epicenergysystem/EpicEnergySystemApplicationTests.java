package epicenergysystem;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.awt.print.Pageable;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import epicenergysystem.model.Cliente;
import epicenergysystem.model.basic.TipoCliente;
import epicenergysystem.service.impl.ClienteServiceImpl;
import epicenergysystem.service.impl.RuoloServiceImpl;
import epicenergysystem.service.impl.TipoClienteServiceImpl;
import epicenergysystem.service.impl.UtenteServiceImpl;

@SpringBootTest 
class EpicEnergySystemApplicationTests {

	@Autowired
	UtenteServiceImpl utenteImpl;
	
	@Autowired
	ClienteServiceImpl clienteImpl;
	
	@Autowired
	RuoloServiceImpl ruoloImpl;
	
	@Autowired
	TipoClienteServiceImpl tipoImpl;
	
	TipoCliente t;
	Cliente c;
	Pageable pageable;
	
	@BeforeEach
	public void create() {
		t = tipoImpl.getByStatoCliente("SRL");
		
		c = Cliente.builder()
				.ragioneSociale("Enel")
				.partitaIva("123545345")
				.email("enel@energia.com")
				.pec("enel@energia.com")
				.telefono("947162932")
				.tipoCliente(t)
				.build();
	}
	
	@Test
	@DisplayName("Aggiungere cliente")
	void testSaveCliente() {
		var cSave = clienteImpl.save(c);
		var cSearch = clienteImpl.getById(cSave.getId());
		assertEquals(cSearch.getId(), clienteImpl.getById(cSearch.getId()).getId(), "gli utenti devono essere uguali");
		clienteImpl.delete(cSearch.getId()); 
	}
	
	@Test
	@DisplayName("Eliminare cliente")
	void testDeleteCliente() {
		
		var cSave = clienteImpl.save(c);
		var d = clienteImpl.delete(cSave.getId());
		assertEquals(cSave.getId(), d.getId(), "l'utente che elimino deve essere lo stesso che salvo");
	}
	
	@Test
	@DisplayName("Controllo email salvata utente")
	void testSaveUtente() {
		var cSave = clienteImpl.save(c);
		assertEquals(cSave.getEmail(), clienteImpl.getByEmail("enel@energia.com").getEmail(), "la mail deve corrispondere");
		clienteImpl.delete(cSave.getId());
	} 
	
	@Test
	@DisplayName("Controllo della paginazione")
	public void testGetAllByNome() {
		assertThat(clienteImpl.findAllByOrderByDataUltimoContatto(0, 1,"ASC", "dataUltimoContatto").getSize()).isEqualTo(1);
	} 
			
	
}
