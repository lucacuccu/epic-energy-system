package epicenergysystem.runner;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import epicenergysystem.loader_file.repo.ComuneRepository;
import epicenergysystem.loader_file.repo.ProvinciaRepository;
import epicenergysystem.repository.ClienteRepository;
import epicenergysystem.repository.FatturaRepository;
import epicenergysystem.repository.IndirizzoRepository;
import epicenergysystem.repository.StatoRepository;
import epicenergysystem.service.impl.ChangeDate;
import epicenergysystem.service.impl.FatturaServiceImpl;
import epicenergysystem.service.impl.IndirizzoServiceImpl;

@Component
public class TestRunner implements CommandLineRunner{
	
	@Autowired
	ClienteRepository clienteRepo;
	
	@Autowired
	IndirizzoServiceImpl indirizzoImpl;
	
	@Autowired
	ComuneRepository comuneRepo;
	
	@Autowired
	ProvinciaRepository provinciaRepo;
	
	@Autowired
	StatoRepository statoRepo;
	
	@Autowired
	FatturaRepository fatturaRepo;
	
	@Autowired
	FatturaServiceImpl fatturaImpl;
	
	@Autowired
	ChangeDate change;
	
	@Autowired
	IndirizzoRepository indirizzoRepo;

	@Override
	public void run(String... args) throws Exception {
		
//		var c = clienteRepo.findById(1);
//		
//		var s = statoRepo.findById(1);
//	
//		Fattura f = Fattura.builder()
//				.importo(BigDecimal.valueOf(134.21))
//				.numeroFattura(1)
//				.anno(2021)
//				.build();
//		
//		var fff = fatturaImpl.save(1,1, f);
		
//		var p = provinciaRepo.findBySigla("CA");
//		
//		Comune cc = comuneRepo.findByNomeAndProvince("Assemini", p.get());
//		
//		Indirizzo i = Indirizzo.builder()
//				.tipo(TipoIndirizzo.SEDE_OPERATIVA)
//				.via("Corso Africa")
//				.civico("10")
//				.cap("09032")
//				.comune(cc)
//				.build();
//			
//		indirizzoImpl.save(1, i);
		
		
	}
	
	
	

}
