package epicenergysystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.model.ContattoCliente;
import epicenergysystem.pojo_models.ContattoClientePojo;
import epicenergysystem.service.impl.ClienteServiceImpl;
import epicenergysystem.service.impl.ContattoClienteServiceImpl;

@RestController
@RequestMapping("contattocliente")
public class ContattoClienteController {
	 
	@Autowired
	ClienteServiceImpl clienteImpl;
	
	@Autowired
	ContattoClienteServiceImpl contattoClienteImpl;

	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ContattoCliente save(@RequestBody ContattoClientePojo contattoPojo) {
		
		ContattoCliente contatto = ContattoCliente.builder() 
				.nome(contattoPojo.getNome())
				.cognome(contattoPojo.getCognome())
				.email(contattoPojo.getEmail())
				.telefono(contattoPojo.getTelefono())
				.cliente(clienteImpl.getById(contattoPojo.getIdCliente()))
				.build();
		
		return contattoClienteImpl.save(contatto);
	}
	 
	@PostMapping("/edit")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ContattoCliente edit(@RequestBody ContattoClientePojo contattoPojo) {
		ContattoCliente contatto = ContattoCliente.builder()
				.nome(contattoPojo.getNome())
				.cognome(contattoPojo.getCognome())
				.email(contattoPojo.getEmail())
				.telefono(contattoPojo.getTelefono())
				.cliente(clienteImpl.getById(contattoPojo.getIdCliente()))
				.build();
		contatto.setId(contattoPojo.getId());
		return contattoClienteImpl.edit(contatto); 
	}
	
	@DeleteMapping("/delete/{idContatto}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ContattoCliente delete(@PathVariable int idContatto) {
		return contattoClienteImpl.delete(idContatto);
	}
	
	@PostMapping("/getbyemail")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ContattoCliente getByEmail(@RequestBody String email) {
		return contattoClienteImpl.getByEmail(email);
	}
	
	@PostMapping("/getbycliente/{idCliente}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<ContattoCliente> getByCliente(@PathVariable int idCliente) {
		return contattoClienteImpl.getByCliente(idCliente);
	}
	
	@GetMapping("/getallsort")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<ContattoCliente> getAllt(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "8") Integer size, @RequestParam(defaultValue = "asc") String dir, @RequestParam(defaultValue = "id") String sort) {
		return contattoClienteImpl.getAll(page, size, dir, sort);
	}
	
}
