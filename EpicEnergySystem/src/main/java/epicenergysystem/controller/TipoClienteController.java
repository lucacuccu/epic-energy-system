package epicenergysystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.model.basic.TipoCliente;
import epicenergysystem.service.impl.TipoClienteServiceImpl;

@RestController
@RequestMapping("tipocliente")
public class TipoClienteController {

	@Autowired
	TipoClienteServiceImpl tipoImpl;
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public TipoCliente save(@RequestBody TipoCliente tipoCliente) {
		return tipoImpl.save(tipoCliente);
	} 

	@PostMapping("/edit")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public TipoCliente edit(@RequestBody TipoCliente tipoCliente) {
		return tipoImpl.edit(tipoCliente);
	}
	
	@DeleteMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public TipoCliente delete(@RequestBody TipoCliente tipoCliente) {
		return tipoImpl.delete(tipoCliente);
	}
	
	@GetMapping("/getbystato")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public TipoCliente getByStato(@RequestBody TipoCliente statoCliente) {
		return tipoImpl.getByTipoCliente(statoCliente);
	}
	
}