package epicenergysystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.loader_file.service.impl.ComuneServiceImpl;
import epicenergysystem.model.Cliente;
import epicenergysystem.model.Indirizzo;
import epicenergysystem.model.SedeIndirizzo;
import epicenergysystem.pojo_models.IndirizzoPojo;
import epicenergysystem.repository.IndirizzoRepository.ClientePerProvincia;
import epicenergysystem.service.impl.ClienteServiceImpl;
import epicenergysystem.service.impl.IndirizzoServiceImpl;

@RestController
@RequestMapping("/indirizzo")
public class IndirizzoController {
	
	@Autowired
	IndirizzoServiceImpl indirizzoImpl;
	
	@Autowired
	ComuneServiceImpl comuneImpl;
	
	@Autowired
	ClienteServiceImpl clienteImpl;
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Indirizzo save(@RequestBody IndirizzoPojo indirizzoPojo) {
		Indirizzo indirizzo = Indirizzo.builder()
				.tipo(SedeIndirizzo.valueOf(indirizzoPojo.getTipoIndirizzo()))
				.via(indirizzoPojo.getVia())
				.civico(indirizzoPojo.getCivico())		
				.localita(indirizzoPojo.getLocalita())
				.cap(indirizzoPojo.getCap())
				.comune(comuneImpl.getFindComuneByProvincia(indirizzoPojo.getNomeComune(), indirizzoPojo.getSiglaProvincia()))
				.cliente(clienteImpl.getById(indirizzoPojo.getIdCliente()))
				.build();
		return indirizzoImpl.save(indirizzo);
	}
	
	@PostMapping("/edit")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Indirizzo edit(@RequestBody IndirizzoPojo indirizzoPojo) {
		Indirizzo indirizzo = Indirizzo.builder()
				.tipo(SedeIndirizzo.valueOf(indirizzoPojo.getTipoIndirizzo()))
				.via(indirizzoPojo.getVia())
				.civico(indirizzoPojo.getCivico())		
				.localita(indirizzoPojo.getLocalita())
				.cap(indirizzoPojo.getCap())
				.cliente(clienteImpl.getById(indirizzoPojo.getIdCliente()))
				.build();
		return indirizzoImpl.edit(indirizzo);
	}
	
	@DeleteMapping("/delete/{indirizzo}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Indirizzo edit(@PathVariable int indirizzo) {
		return indirizzoImpl.delete(indirizzo);
	}
	
	@PostMapping("/getallbysort")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Indirizzo> getAll(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "8") Integer size, @RequestParam(defaultValue = "ASC") String dir, @RequestParam(defaultValue = "id") String sort) {
		return indirizzoImpl.getAll(size, page, dir, sort);
	}

}
