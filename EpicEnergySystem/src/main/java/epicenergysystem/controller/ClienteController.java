package epicenergysystem.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.model.Cliente;
import epicenergysystem.pojo_models.ClientePojo;
import epicenergysystem.repository.ClienteRepository.ImportoPerAnno;
import epicenergysystem.repository.IndirizzoRepository.ClientePerProvincia;
import epicenergysystem.service.impl.ClienteServiceImpl;
import epicenergysystem.service.impl.IndirizzoServiceImpl;
import epicenergysystem.service.impl.TipoClienteServiceImpl;

@RestController
@RequestMapping("cliente")
public class ClienteController {
	
	@Autowired
	ClienteServiceImpl clienteImpl;
	
	@Autowired
	TipoClienteServiceImpl tipoClienteImpl; 
	
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Cliente save(@RequestBody ClientePojo clientePojo) {
		Cliente cliente = Cliente.builder()
				.ragioneSociale(clientePojo.getRagioneSociale())
				.partitaIva(clientePojo.getPartitaIva())
				.email(clientePojo.getEmail())
				.pec(clientePojo.getPec())
				.telefono(clientePojo.getTelefono())
				.tipoCliente(tipoClienteImpl.getByStatoCliente(clientePojo.getTipoCliente()))
				.build();
		return clienteImpl.save(cliente);
	}
	 
	@PostMapping("/edit")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Cliente edit(@RequestBody ClientePojo clientePojo) {
		Cliente cliente = Cliente.builder()
				.ragioneSociale(clientePojo.getRagioneSociale())
				.partitaIva(clientePojo.getPartitaIva())
				.email(clientePojo.getEmail())
				.pec(clientePojo.getPec())
				.telefono(clientePojo.getTelefono())
				.tipoCliente(tipoClienteImpl.getByStatoCliente(clientePojo.getTipoCliente()))
				.build();
		cliente.setId(clientePojo.getId());
		return clienteImpl.edit(cliente);
	}
	
	@DeleteMapping("/delete/{idCliente}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Cliente delete(@PathVariable int idCliente) {
		return clienteImpl.delete(idCliente);
	}
	
	@GetMapping("/getbyemail/{email}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Cliente getByEmail(@PathVariable String email) {
		return clienteImpl.getByEmail(email);
	}

	@GetMapping("/getbypec/{pec}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Cliente getByPec(@PathVariable String pec) {
		return clienteImpl.getByPec(pec);
	}

	@GetMapping("/getbytipocliente/{tipoCliente}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Cliente> getByTipoCliente(@PathVariable String tipoCliente) {
		return clienteImpl.getByTipoCliente(tipoCliente);
	}

	@GetMapping("/getbyimportoanno/{idCliente}/{anno}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public BigDecimal getImportoByAnno(@PathVariable int idCliente, @PathVariable int anno) {
		return clienteImpl.getImportoByAnno(idCliente, anno);
	}
	
	@GetMapping("/getbyimportianno/{anno}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<ImportoPerAnno> getImportiByAnno(@PathVariable int anno) {
		return clienteImpl.getImportiByAnno(anno);
	}
	
	@GetMapping("/getbydatainserimentoorder")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Cliente> getByDataInserimentoOrder(){
		return clienteImpl.getByDataInserimentoOrder();
	}
	
	@GetMapping("/getallbyultimocontattogroup")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Cliente> findByDataInserimentoGroup(@RequestParam(defaultValue = "0") Integer page, 
			@RequestParam(defaultValue = "8") Integer size, @RequestParam(defaultValue = "ASC") String dir, @RequestParam(defaultValue = "id") String sort) {
		try {			
			return clienteImpl.findAllByOrderByDataUltimoContatto(page, size, dir, sort);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@GetMapping("/getcontainsragionesociale/{ragioneSociale}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Cliente> getContainsRagioneSociale(@PathVariable String ragioneSociale) { 
		return clienteImpl.getByContainsRagioneSociale(ragioneSociale);
	}
	
	@GetMapping("/getbyrangedate/{start}/{end}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Cliente> getByRangeData(@PathVariable String start, @PathVariable String end) throws ParseException {
		
		Date st = new SimpleDateFormat("yyyy-MM-dd").parse(start);
		Date en = new SimpleDateFormat("yyyy-MM-dd").parse(end);
		
		return clienteImpl.getByRangeDataInserimento(st, en); 
		
	}
	
	@GetMapping("/getbyannoultimocontatto/{anno}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Cliente> getByAnnoUltimoContattoContains(@PathVariable int anno) {
		return clienteImpl.getByAnnoUltimoContattoContains(anno);
	}	
	
	@GetMapping("/getbydataultimocontatto/{dataUltimoContatto}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Cliente> getByDataUltimoContatto(@PathVariable String dataUltimoContatto) throws ParseException {
		
		Date st = new SimpleDateFormat("yyyy-MM-dd").parse(dataUltimoContatto);
		
		return clienteImpl.getByDataUltimoContatto(st);
		
	}  
	
	@GetMapping("/getallbysigla/{sigla}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<ClientePerProvincia> getBySigla(@PathVariable String sigla) {
		return clienteImpl.getBySigla(sigla);
	}

}
