package epicenergysystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.security.model.Utente;
import epicenergysystem.service.impl.UtenteServiceImpl;

@RestController
@RequestMapping("/utente")
public class UtenteController {
	
	@Autowired
	UtenteServiceImpl utenteImpl;
	
	@Autowired
	PasswordEncoder encoder;
	
	@GetMapping(value = "/getallutentebysort")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Utente> getAllUtentePageSort(@RequestParam(defaultValue = "0") Integer page,
				@RequestParam(defaultValue = "8") Integer size, @RequestParam(defaultValue = "ASC") String dir, @RequestParam(defaultValue = "id") String sort) {
		return utenteImpl.getAllUtenteBySort(page, size, dir, sort);
		 
	} 
	
	@PostMapping("/save/{idRuolo}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Utente salvaUtente(@PathVariable int idRuolo,@RequestBody Utente utente) {
		return utenteImpl.save(idRuolo, utente);
	}
	
	@PostMapping("/savefirst/{idRuolo}")
	public Utente salvaPrimoUtente(@RequestBody Utente utente, @PathVariable int idRuolo) {
		return utenteImpl.save(idRuolo, utente);
	}
	
	@DeleteMapping("/delete/{idUtente}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Utente salvaUtente(@PathVariable int idUtente) {
		return utenteImpl.delete(idUtente);
	}

}
