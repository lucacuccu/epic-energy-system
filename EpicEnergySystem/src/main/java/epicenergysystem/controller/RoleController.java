package epicenergysystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.security.model.Ruolo;
import epicenergysystem.service.impl.RuoloServiceImpl;

@RestController
@RequestMapping("/role")
public class RoleController {
	
	@Autowired
	RuoloServiceImpl roleImpl;
	
	@GetMapping("/getallrolebysort")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Ruolo> getAllRolePageSort(@RequestParam(defaultValue = "0") Integer page,
				@RequestParam(defaultValue = "8") Integer size, @RequestParam(defaultValue = "id") String sort) {
		// sort -> indica il nome dell'attributo
		return roleImpl.getAllRoleBySort(page, size, sort);
	}
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Ruolo saveRole(@RequestBody Ruolo role) {
		return roleImpl.save(role);
	}
	
	@PostMapping("/savefirst")
	public Ruolo saveFirstRole(@RequestBody Ruolo role) {
		return roleImpl.save(role);
	}

}
