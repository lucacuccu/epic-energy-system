package epicenergysystem.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.model.Fattura;
import epicenergysystem.pojo_models.FatturaPojo;
import epicenergysystem.service.impl.ClienteServiceImpl;
import epicenergysystem.service.impl.FatturaServiceImpl;
import epicenergysystem.service.impl.StatoFatturaServiceImpl;

@RestController
@RequestMapping("fattura")
public class FatturaController {
	 
	@Autowired
	FatturaServiceImpl fatturaImpl;
	
	@Autowired
	StatoFatturaServiceImpl statoFatturaImpl;
	
	@Autowired
	ClienteServiceImpl clienteImpl;
	 
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')") 
	public Fattura save(@RequestBody FatturaPojo fatturaPojo) throws ParseException {
		Fattura fattura = Fattura.builder() 
				.dataInserimento(new SimpleDateFormat("yyyy-MM-dd").parse(fatturaPojo.getData()))	
				.anno(fatturaPojo.getAnno())
				.numeroFattura(fatturaPojo.getNumeroFattura())
				.importo(fatturaPojo.getImporto())
				.stato(statoFatturaImpl.getByStato(fatturaPojo.getNomeStato()))
				.cliente(clienteImpl.getById(fatturaPojo.getIdCliente()))
				.build();
		return fatturaImpl.save(fattura);
	}
	
	@PostMapping("/edit")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Fattura edit(@RequestBody FatturaPojo fatturaPojo) throws ParseException {
		Fattura fattura = Fattura.builder()
				.anno(fatturaPojo.getAnno())
				.numeroFattura(fatturaPojo.getNumeroFattura())
				.importo(fatturaPojo.getImporto())
				.stato(statoFatturaImpl.getByStato(fatturaPojo.getNomeStato()))
				.cliente(clienteImpl.getById(fatturaPojo.getIdCliente()))
				.build();
		if(fatturaPojo.getData()!=null) {
			fattura.setDataInserimento(new SimpleDateFormat("yyyy-MM-dd").parse(fatturaPojo.getData()));
		}
		fattura.setId(fatturaPojo.getId());
		return fatturaImpl.edit(fattura);
	}
	
	@DeleteMapping("/delete/{idFattura}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Fattura edit(@PathVariable int idFattura) {
		return fatturaImpl.delete(idFattura);
	}
	
	@GetMapping("/getbyrange/{rangeStart}/{rangeEnd}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	List<Fattura> getByRangeImporto(@PathVariable BigDecimal rangeStart, @PathVariable BigDecimal rangeEnd){
		return fatturaImpl.getByRangeImporto(rangeStart, rangeEnd);
	}
	
	@GetMapping("/getbyrangeforcliente/{idCliente}/{rangeStart}/{rangeEnd}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	List<Fattura> getByRangeImporto(@PathVariable int idCliente, @PathVariable BigDecimal rangeStart, @PathVariable BigDecimal rangeEnd){
		return fatturaImpl.getByRangeImportoForCLiente(idCliente, rangeStart, rangeEnd);
	}
	
	@GetMapping("/getbyanno/{idCliente}/{anno}")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public List<Fattura> getByAnno(@PathVariable int idCliente, @PathVariable int anno) {
		return fatturaImpl.getByAnno(idCliente, anno);
	}

	@GetMapping("/getallfatturasort")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Fattura> getAllBySort(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "8") Integer size, @RequestParam(defaultValue = "ASC") String dir, @RequestParam(defaultValue = "id") String sort) {
		return fatturaImpl.getAll(page, size, dir, sort);
	}
	
	

}
