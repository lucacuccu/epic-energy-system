package epicenergysystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.model.basic.StatoFattura;
import epicenergysystem.service.impl.StatoFatturaServiceImpl;

@RestController
@RequestMapping("stato")
public class StatoController {
	
	@Autowired
	StatoFatturaServiceImpl statoImpl;
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public StatoFattura save(@RequestBody StatoFattura stato) {
		return statoImpl.save(stato);
	}
	

	@PostMapping("/edit")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public StatoFattura edit(@RequestBody StatoFattura stato) {
		return statoImpl.edit(stato);
	}
	
	@DeleteMapping("/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public StatoFattura delete(@RequestBody StatoFattura stato) {
		statoImpl.delete(stato.getStatoFattura());
		return statoImpl.getByStatoFattura(stato);
	}
	
	@GetMapping("/getbystato")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public StatoFattura getByStato(@RequestBody StatoFattura stato) {
		return statoImpl.getByStatoFattura(stato);
	}

}
