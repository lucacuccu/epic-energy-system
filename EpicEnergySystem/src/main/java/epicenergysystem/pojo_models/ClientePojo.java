package epicenergysystem.pojo_models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientePojo {
	
	private int id;
	private String ragioneSociale;
	private String partitaIva;
	private String email;
	private String pec;
	private String telefono;
	private String tipoCliente;

}
