package epicenergysystem.pojo_models;

import lombok.Data;

@Data
public class ContattoClientePojo {
	 
	private int id;
	private String email;
	private String nome;
	private String cognome;
	private String telefono;
	private int idCliente;
	

}
