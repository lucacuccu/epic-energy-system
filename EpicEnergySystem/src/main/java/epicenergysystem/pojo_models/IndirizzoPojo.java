package epicenergysystem.pojo_models;

import lombok.Data;

@Data
public class IndirizzoPojo {
	
	int id;
	private String tipoIndirizzo;
	private String via;
	private String civico;
	private String localita;
	private String cap;
	private String nomeComune;
	private String siglaProvincia;
	private int idCliente;

}
