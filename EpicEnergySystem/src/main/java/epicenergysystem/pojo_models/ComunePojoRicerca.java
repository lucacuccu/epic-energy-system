package epicenergysystem.pojo_models;

import lombok.Data;

@Data
public class ComunePojoRicerca {

	private String nomeComune;
	private String siglaProvincia;
	
}
