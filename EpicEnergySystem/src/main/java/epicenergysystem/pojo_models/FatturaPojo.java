package epicenergysystem.pojo_models;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class FatturaPojo {
	 
	private int id;
	
	private String data;
	private int anno;
	private Integer numeroFattura;
	private BigDecimal importo;
	private String nomeStato;
	private int idCliente;

}
