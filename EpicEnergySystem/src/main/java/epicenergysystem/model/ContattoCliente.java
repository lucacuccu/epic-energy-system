package epicenergysystem.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import epicenergysystem.cript.Convertitore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Contatto di un Cliente.
 * 
 * @author Luca Cuccu
 *
 */
@Data
@Entity 
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ContattoCliente extends BaseEntity {

	@Convert(converter = Convertitore.class)
	@Column(unique = true)
	@NotNull
	private String email;
	@Convert(converter = Convertitore.class)
	private String nome;
	@Convert(converter = Convertitore.class)
	private String cognome;
	@Convert(converter = Convertitore.class)
	@NotNull
	private String telefono;
	
	@ManyToOne
	private Cliente cliente;
	
}
