package epicenergysystem.model.basic;

import javax.persistence.Column;
import javax.persistence.Entity;

import epicenergysystem.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class TipoCliente extends BaseEntity {
	@Column(unique = true)
	private String statoCliente;	
}
