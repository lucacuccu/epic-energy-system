package epicenergysystem.model.basic;

import javax.persistence.Column;
import javax.persistence.Entity;

import epicenergysystem.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class StatoFattura extends BaseEntity {
	@Column(unique = true)
	private String statoFattura;
}
