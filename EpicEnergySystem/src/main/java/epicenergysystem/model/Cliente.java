package epicenergysystem.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import epicenergysystem.cript.Convertitore;
import epicenergysystem.model.basic.TipoCliente;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Classe Cliente.
 * 
 * @author Luca Cuccu
 *
 */
@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor 
@NoArgsConstructor
public class Cliente extends BaseEntity {
	
	@Column(nullable = false, length = 50)
	private String ragioneSociale;
	@Column(unique = true, length = 11, nullable = false)
	private String partitaIva;
	@Column(unique = true, nullable = false, length = 80)
	@Convert(converter = Convertitore.class)
	private String email;
	@Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", 
			insertable = false, 
			updatable = false)
	private Date dataInserimento;
	@Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", 
			insertable = false, 
			updatable = true)
	private Date dataUltimoContatto;
	@Convert(converter = Convertitore.class)
	@Column(unique = true, length = 80)
	private String pec;
	@Convert(converter = Convertitore.class)
	@Column(nullable = false, length = 24)
	private String telefono;
	
	@ManyToOne
	private TipoCliente tipoCliente;
	
}