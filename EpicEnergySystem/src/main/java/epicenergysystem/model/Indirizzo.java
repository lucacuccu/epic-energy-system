package epicenergysystem.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import epicenergysystem.model.entityfromfile.Comune;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Indirizzo di un Cliente.
 * 
 * @author Luca Cuccu
 *
 */
@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Indirizzo extends BaseEntity {
	
	@Enumerated(EnumType.STRING)
	private SedeIndirizzo tipo;
	
	private String via;
	private String civico;
	private String localita;
	private String cap;
	
	@ManyToOne
	private Comune comune;
	
	@ManyToOne
	private Cliente cliente;

}