package epicenergysystem.model;

/**
 * Tipo sede di un Indirizzo.
 * 
 * @author Luca Cuccu
 *
 */
public enum SedeIndirizzo{
	SEDE_LEGALE, SEDE_OPERATIVA 
}
