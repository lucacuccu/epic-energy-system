package epicenergysystem.model.entityfromfile;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import epicenergysystem.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Comune.
 * 
 * @author Luca Cuccu
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Comune extends BaseEntity implements Comparable<Comune> {

	private String nome;
	@ManyToOne
	private Provincia province;
		
	@Override
	public String toString() {
		return String.format("%s (%s)", nome, province.getSigla());
	}

	@Override
	public int compareTo(Comune o) {
		// confrontiamo le province
		var p = province.getSigla().compareTo(o.province.getSigla());
		// se la provincia e la stessa (p==0)
		// confronto i nomi, altrimenti vale il confronto tra province
		return p == 0 ? nome.compareTo(o.nome) : p;
	}
	
}
