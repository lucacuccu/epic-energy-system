package epicenergysystem.model.entityfromfile;

import javax.persistence.Column;
import javax.persistence.Entity;

import epicenergysystem.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Provincia.
 * 
 * @author Luca Cuccu
 *
 */
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Provincia extends BaseEntity{
	
	@Column(unique = true)
	private String nome;
	@Column(unique = true, length = 3)
	private String sigla;

}
