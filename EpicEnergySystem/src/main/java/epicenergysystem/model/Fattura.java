package epicenergysystem.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.Id;

import epicenergysystem.model.basic.StatoFattura;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Fattura di un Cliente.
 * 
 * @author Luca Cuccu
 *
 */
@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Fattura extends BaseEntity{
	
	private Date dataInserimento;
	private Integer anno;
	@Column(nullable = false)
	private BigDecimal importo;
	private Integer numeroFattura;
	
	@ManyToOne
	private StatoFattura stato;
	
	@ManyToOne
	private Cliente cliente;	
	
}
