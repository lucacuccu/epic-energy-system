package epicenergysystem.exceptions;

public class AppServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AppServiceException() {}
	
	public AppServiceException(String e) {
		super(e);
	}
	
	public AppServiceException(Throwable e) {
		super(e); 
	}
	
}
