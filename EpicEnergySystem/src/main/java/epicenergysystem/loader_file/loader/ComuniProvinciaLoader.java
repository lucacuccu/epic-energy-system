package epicenergysystem.loader_file.loader;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epicenergysystem.loader_file.repo.ComuneRepository;
import epicenergysystem.loader_file.repo.ProvinciaRepository;
import epicenergysystem.loader_file.service.ComuneProvinciaService;
import epicenergysystem.model.entityfromfile.Comune;
import epicenergysystem.model.entityfromfile.Provincia;

@Service
public class ComuniProvinciaLoader implements ComuneProvinciaService {
	
	@Autowired
	ComuneRepository comuneRepo; 
	
	@Autowired
	ProvinciaRepository provinciaRepo; 
	
	@Override
	public Set<Comune> load(InputStream is) {
		
		var comuni = new HashSet<Comune>();
		
		try (var scanner = new Scanner(is)){	// apre uno scanner sullo stream di input
			// devo leggere riga per riga il file
			// - mi sono ricordato che ci sono 3 righe di intestazione
			for (var i = 0; i < 3; ++i)
				scanner.nextLine();	// lo scarto semplicemente
			while(scanner.hasNextLine()) {	//quando ci sono ancora linee da leggere // per ogni riga devo prendere le informazioni che mi servono
				// leggo la prossima riga
				String riga = scanner.nextLine();
				// devo prendere le informazioni che mi servono
				String[]parts = riga.split(";");	//spezzo la linea in tante parti separate da ;
				// citta
				String nomeCitta = parts[5];
				// provincia
				String nomeProvincia = parts[11];
				String siglaProvincia = parts[14];
				
				var provincia = new Provincia(nomeProvincia, siglaProvincia);
				
				// String name, Province province
				var comune = new Comune();
				comune.setNome(nomeCitta);
				comune.setProvince(provincia);
				// devo inserire le citta nella lista che produrro come output
				 
				comuni.add(comune);
			}
			
		} catch (Exception e) {
			
		}
		// alla fine del file dovro restituire la lista costruita
		return comuni;
	}

}
