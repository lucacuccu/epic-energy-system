package epicenergysystem.loader_file;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import epicenergysystem.loader_file.loader.ComuniProvinciaLoader;
import epicenergysystem.loader_file.service.impl.ComuneServiceImpl;
import epicenergysystem.model.entityfromfile.Comune;
import epicenergysystem.model.entityfromfile.Provincia;

@RestController
@RequestMapping("comuniprovince")
public class ComuniProvinceController {
	
	@Autowired
	ComuniProvinciaLoader cpLoader;
	
	@Autowired
	ComuneServiceImpl comuneImpl;
	
	@GetMapping("/saveall")
	@PreAuthorize("hasRole('ROLE_MAINTAINER')")
	public void save() throws MalformedURLException, IOException {
		try {
			var file = "C:\\Users\\lucac\\git\\epic_energy_system\\EpicEnergySystem\\Elenco-comuni-italiani.csv";
			
			var comuni = cpLoader.load(new FileInputStream(file));
					 
			comuni.stream().map(c -> Comune.builder() //
					.nome(c.getNome()) //
					.province(Provincia.builder() //
							.nome(c.getProvince().getNome()) //
							.sigla(c.getProvince().getSigla()) //
							.build()) //
					.build()) //
			.forEach(c -> comuneImpl.save(c))
			; 
			
			
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
