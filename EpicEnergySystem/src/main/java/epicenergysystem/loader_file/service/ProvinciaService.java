package epicenergysystem.loader_file.service;

import epicenergysystem.model.entityfromfile.Provincia;

public interface ProvinciaService {
	
	Provincia save(Provincia provincia);
	Provincia getBySigla(Provincia provincia);
	Provincia getByNome(Provincia provincia);

}
