package epicenergysystem.loader_file.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.loader_file.repo.ProvinciaRepository;
import epicenergysystem.loader_file.service.ProvinciaService;
import epicenergysystem.model.entityfromfile.Provincia;

@Service
public class ProvinciaServiceImpl implements ProvinciaService {
	
	@Autowired
	ProvinciaRepository provinciaRepo;

	@Override
	public Provincia save(Provincia provincia) {
		try {
			return provinciaRepo.save(provincia);
		} catch (Exception e) {
			throw new AppServiceException("Provincia non salvata");
		}
	}

	@Override
	public Provincia getBySigla(Provincia provincia) {
		try {
			return provinciaRepo.findBySigla(provincia.getSigla()).get();
		} catch (Exception e) {
			throw new AppServiceException("Provincia non trovata");
		}
	}

	@Override
	public Provincia getByNome(Provincia provincia) {
		try {
			return provinciaRepo.findByNome(provincia.getNome());
		} catch (Exception e) {
			throw new AppServiceException("Provincia non trovata");
		}
	}
	
	

}
