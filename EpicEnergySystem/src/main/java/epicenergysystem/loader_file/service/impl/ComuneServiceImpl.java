package epicenergysystem.loader_file.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.loader_file.repo.ComuneRepository;
import epicenergysystem.loader_file.repo.ProvinciaRepository;
import epicenergysystem.loader_file.service.ComuneService;
import epicenergysystem.model.entityfromfile.Comune;
import epicenergysystem.model.entityfromfile.Provincia;

@Service
public class ComuneServiceImpl implements ComuneService {
	
	@Autowired
	ProvinciaRepository provinciaRepo;
	
	@Autowired
	ComuneRepository comuneRepo;
	
	@Override
	public Comune getFindComuneByProvincia(String nome, String sigla) {
		try {
			var p = provinciaRepo.findBySigla(sigla);
			return comuneRepo.findByNomeAndProvince(nome, p.get());
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}
	
	@Override
	public Comune save(Comune comune) {
		try {
			Provincia provincia;
			// recupero la provincia dal database
			var p = provinciaRepo.findBySigla(comune.getProvince().getSigla());
				// se non c'e la inserisco
				if(p.isEmpty()) provincia = provinciaRepo.save(comune.getProvince());
				// altrimenti prendo quella recuperata
				else provincia = p.get();
			// alla fine metto il risultato della precedente istruzione nella provincia
			comune.setProvince(provincia);
			return comuneRepo.save(comune);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}
	
}

