package epicenergysystem.loader_file.service;

import epicenergysystem.model.entityfromfile.Comune;

public interface ComuneService {

	Comune save(Comune comune);

	Comune getFindComuneByProvincia(String nome, String sigla);

}
