package epicenergysystem.loader_file.service;

import java.io.InputStream;
import java.util.Set;

import epicenergysystem.model.entityfromfile.Comune;


public interface ComuneProvinciaService {

	Set<Comune> load(InputStream is);
	
}
