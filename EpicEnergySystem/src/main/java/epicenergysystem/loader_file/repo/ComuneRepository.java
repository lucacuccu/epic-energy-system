package epicenergysystem.loader_file.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import epicenergysystem.model.entityfromfile.Comune;
import epicenergysystem.model.entityfromfile.Provincia;

public interface ComuneRepository extends JpaRepository<Comune, Integer> {
	
	Comune findByNomeAndProvince(String nome, Provincia province);
	
}
