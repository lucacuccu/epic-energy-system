package epicenergysystem.loader_file.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import epicenergysystem.model.entityfromfile.Provincia;



public interface ProvinciaRepository extends JpaRepository<Provincia, Integer>{

	Optional<Provincia> findBySigla(String sigla);
	Provincia findByNome(String nome);
	
}
