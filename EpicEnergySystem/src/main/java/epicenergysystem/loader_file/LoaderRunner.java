package epicenergysystem.loader_file;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import epicenergysystem.loader_file.loader.ComuniProvinciaLoader;
import epicenergysystem.loader_file.service.impl.ComuneServiceImpl;
import epicenergysystem.model.entityfromfile.Comune;
import epicenergysystem.model.entityfromfile.Provincia;

@Component
public class LoaderRunner implements CommandLineRunner{
	
	@Autowired
	ComuniProvinciaLoader cpLoader;
	
	@Autowired
	ComuneServiceImpl comuneImpl;

	@Override
	public void run(String... args) throws Exception, MalformedURLException, IOException {
		
		var file = "C:\\Users\\lucac\\git\\epic_energy_system\\EpicEnergySystem\\Elenco-comuni-italiani.csv";
		
		var comuni = cpLoader.load(new FileInputStream(file)); 
				 
		comuni.stream().map(c -> Comune.builder() //
				.nome(c.getNome()) //
				.province(Provincia.builder() //
						.nome(c.getProvince().getNome()) //
						.sigla(c.getProvince().getSigla()) //
						.build()) //
				.build()) //
		//.forEach(c -> comuneImpl.save(c))
		; 
		
	}

}