package epicenergysystem.security.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

import epicenergysystem.cript.Convertitore;
import epicenergysystem.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Luca Cuccu
 */

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Utente extends BaseEntity {
	
	@Column(unique = true, nullable = false)
	@Convert(converter = Convertitore.class)
	private String username;
	private String nome;
	private String cognome;
	@Column(unique = true, nullable = false)
	@Convert(converter = Convertitore.class)
	private String email;
	private String password;
	private Boolean active = true;
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(
	        name="utente_ruolo",
	        joinColumns= @JoinColumn(name="utente_id", referencedColumnName="id"),
	        inverseJoinColumns= @JoinColumn(name="ruolo_id", referencedColumnName="id")
	    )
	private Set<Ruolo> roles = new HashSet<Ruolo>();
		
}
