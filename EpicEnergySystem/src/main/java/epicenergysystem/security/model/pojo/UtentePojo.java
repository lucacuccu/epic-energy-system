package epicenergysystem.security.model.pojo;

import lombok.Data;

@Data
public class UtentePojo {
	
	private String username;
	private String nome;
	private String cognome;
	private String email;
	private String password;
	private Boolean active = true;
	private String ruolo;
	
}
