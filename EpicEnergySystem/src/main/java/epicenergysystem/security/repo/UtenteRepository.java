package epicenergysystem.security.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import epicenergysystem.security.model.Utente;


public interface UtenteRepository extends PagingAndSortingRepository<Utente, Integer> {

	public Optional<Utente> findById(Long id);
	public Optional<Utente> findByUsername(String username);
	public List<Utente> findByNome(String nome);
	public List<Utente> findByEmail(String email);
	public List<Utente> findByPassword(String password);
	public List<Utente> findByActive(boolean active);
	
	// getAll()
	public List<Utente> findAll();
	
}
