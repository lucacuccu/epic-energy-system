package epicenergysystem.security.repo;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import epicenergysystem.security.model.Ruolo;

public interface RuoloRepository extends PagingAndSortingRepository<Ruolo, Integer> {

	Optional<Ruolo> findById(Long id);
	Optional<Ruolo> findByRoleType(String roleType);
	
	// getAll()
	Page<Ruolo> findAll(Pageable pageable);
	
}
