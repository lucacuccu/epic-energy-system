package epicenergysystem.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;

import epicenergysystem.model.Fattura;

/**
 * Gestione di una Fattura.
 * 
 * @author Luca Cuccu
 *
 */
public interface FatturaService {

	/**
	 * Creazione di una fattura
	 * 
	 * @param fattura -> entita fattura
	 *
	 */
	Fattura save(Fattura fattura);
	
	/**
	 * Modifica di una fattura
	 * 
	 * @param fattura -> entita fattura
	 *
	 */
	Fattura edit(Fattura fattura);
	
	/**
	 * Eliminazione di una fattura
	 * 
	 * @param fattura -> id fattura
	 *
	 */
	Fattura delete(int fattura);
	
	/**
	 * Ricerca di un elenco di fatture relative ad uno stato fattura
	 * 
	 * @param stato -> stato fattura
	 *
	 */	
	List<Fattura> getByStato(String stato);
	
	/**
	 * Ricerca di un elenco di fatture relative ad una data ti insimento
	 * 
	 * @param fattura -> entita fattura
	 *
	 */
	List<Fattura> getByDataInserimento(Fattura fattura);
	
	/**
	 * Ricerca di un elenco di fatture relative ad cliente per uno specifico anno
	 * 
	 * @param idCliente -> id cliente scelto
	 * @param anno -> anno da considerare
	 *
	 */	
	List<Fattura> getByAnno(int idCliente, int anno);
	
	/**
	 * Ricerca di un elenco di fatture in un range di importi dati
	 * 
	 * @param rangeStart -> importo minore
	 * @param rangeEnd -> importo maggiore
	 *
	 */	
	List<Fattura> getByRangeImporto(BigDecimal rangeStart, BigDecimal rangeEnd);
	
	/**
	 * Ricerca di un elenco di fatture in un range di importi dati
	 * 
	 * @param idCliente -> id cliente scelto
	 * @param rangeStart -> importo minore
	 * @param rangeEnd -> importo maggiore
	 *
	 */	
	List<Fattura> getByRangeImportoForCLiente(int idCliente, BigDecimal rangeStart, BigDecimal rangeEnd);
	
	/**
	 * Ricerca di tutte le fatture per Paginazione
	 * 
	 * @param page -> grandezza della pagina
	 * @param size -> elementi contenuti nella pagina
	 * @param dir -> direzione di visualizzazione ("ASC" - "DESC")
	 * @param sort -> parametro della classe fattura di visualizzazione
	 *
	 */
	Page<Fattura> getAll(Integer size, Integer page, String dir, String sort);
	
	
	
	
}
