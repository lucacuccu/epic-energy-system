package epicenergysystem.service;

import epicenergysystem.model.basic.TipoCliente;

/**
 * Gestione di un Tipo Cliente.
 * 
 * @author Luca Cuccu
 *
 */
public interface TipoClienteService {
	
	/**
	 * Creazione di un tipo cliente
	 * 
	 * @param tipoCliente -> entita tipo cliente
	 *
	 */
	TipoCliente save(TipoCliente tipoCliente);
	
	/**
	 * Modifica di un tipo cliente
	 * 
	 * @param tipoCliente -> entita tipo cliente
	 *
	 */
	TipoCliente edit(TipoCliente tipoCliente);
	
	/**
	 * Eliminazione di un tipo cliente
	 * 
	 * @param tipoCliente -> entita tipo cliente di cui inserire lo stato del cliente
	 *
	 */
	TipoCliente delete(TipoCliente tipoCliente);

	/**
	 * Ricerca di un tipo cliente 
	 * 
	 * @param statoCliente -> entita tipo cliente
	 *
	 */
	TipoCliente getByTipoCliente(TipoCliente statoCliente);
	
	/**
	 * Ricerca di un tipo cliente tramite una stringa
	 * 
	 * @param statoCliente -> tipo in stringa del tipo cliente 
	 *
	 */
	TipoCliente getByStatoCliente(String statoCliente);

}