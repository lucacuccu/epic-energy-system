package epicenergysystem.service;

import epicenergysystem.model.basic.StatoFattura;

/**
 * Gestione di uno Stato Fattura.
 * 
 * @author Luca Cuccu
 *
 */
public interface StatoFattutaService {
	
	/**
	 * Creazione di uno stato fattura
	 * 
	 * @param stato -> entita stato fattura
	 *
	 */
	StatoFattura save(StatoFattura stato);
	
	/**
	 * Eliminazione di uno stato fattura
	 * 
	 * @param stato -> entita stato fattura
	 *
	 */
	StatoFattura edit(StatoFattura stato);
	
	/**
	 * Eliminazione di uno stato fattura
	 * 
	 * @param nomeStato -> nome dello stato fattura (univoco)
	 *
	 */
	StatoFattura delete(String nomeStato);
	
	/**
	 * Ricerca di uno stato fattura
	 * 
	 * @param stato -> entita stato fattura
	 *
	 */
	StatoFattura getByStatoFattura (StatoFattura stato);
	
	/**
	 * Ricerca di uno stato fattura per stato
	 * 
	 * @param stato -> nome stato fattura
	 *
	 */
	StatoFattura getByStato(String stato);
}
