package epicenergysystem.service;

import org.springframework.data.domain.Page;

import epicenergysystem.security.model.Utente;

/**
 * Gestione di un utente.
 * 
 * @author Luca Cuccu
 *
 */
public interface UtenteService {

	/**
	 * Creazione di un utente
	 * 
	 * @param utente -> entita utente
	 * @param idRuolo -> id ruolo per assegnazione
	 *
	 */
	Utente save(int idRuolo, Utente utente);
	
	/**
	 * Eliminazione di un utente per id
	 * 
	 * @param utente -> id utente
	 *
	 */
	Utente delete(int utente);

	/**
	 * Ricerca di un utente per id
	 * 
	 * @param id -> id utente
	 *
	 */
	Utente findById(int id);
	
	/**
	 * Ricerca di tutti gli utenti per Paginazione
	 * 
	 * @param page -> grandezza della pagina
	 * @param size -> elementi contenuti nella pagina
	 * @param dir -> direzione di visualizzazione ("ASC" - "DESC")
	 * @param sort -> parametro della classe cliente di visualizzazione
	 *
	 */
	Page<Utente> getAllUtenteBySort(Integer page, Integer size, String dir, String sort);

	/**
	 * Ricerca di un utente per username
	 * 
	 * @param username -> username utente
	 *
	 */
	Utente findByIdUsername(String username);

	/**
	 * Metodo per criptare tutte password degli utenti gia salvati nel db
	 */
	void criptAllPasswords();

	
	
}
