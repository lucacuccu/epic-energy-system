package epicenergysystem.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import epicenergysystem.security.model.Ruolo;

/**
 * Gestione di un ruolo.
 * 
 * @author Luca Cuccu
 *
 */
public interface RuoloService {

	/**
	 * Ricerca di tutti i ruoli per Paginazione
	 * 
	 * @param page -> grandezza della pagina
	 * @param size -> elementi contenuti nella pagina
	 * @param dir -> direzione di visualizzazione ("ASC" - "DESC")
	 * @param sort -> parametro della classe ruolo di visualizzazione
	 *
	 */
	Page<Ruolo> getAllRoleBySort(Integer page, Integer size, String sort);

	/**
	 * Creazione di un ruolo
	 * 
	 * @param role -> entita ruolo
	 *
	 */
	Ruolo save(Ruolo role);

	/**
	 * Ricerca di un ruolo Optional per il sui id
	 * 
	 * @param id -> id ruolo
	 *
	 */
	Optional<Ruolo> findById(Long id);

}
