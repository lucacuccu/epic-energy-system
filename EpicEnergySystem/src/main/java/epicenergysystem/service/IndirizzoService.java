package epicenergysystem.service;

import java.util.List;

import org.springframework.data.domain.Page;

import epicenergysystem.model.Cliente;
import epicenergysystem.model.Indirizzo;
import epicenergysystem.pojo_models.ComunePojoRicerca;
import epicenergysystem.repository.IndirizzoRepository.ClientePerProvincia;

/**
 * Gestione di un indirizzo cliente.
 * 
 * @author Luca Cuccu
 *
 */
public interface IndirizzoService {
	
	/**
	 * Creazione di un indirizzo cliente
	 * 
	 * @param indirizzo -> entita indirizzo cliente
	 *
	 */
	Indirizzo save(Indirizzo indirizzo);
	
	/**
	 * Modifica di un indirizzo cliente
	 * 
	 * @param indirizzo -> entita indirizzo cliente
	 *
	 */
	Indirizzo edit(Indirizzo indirizzo);
	
	/**
	 * Eliminazione di un indirizzo cliente
	 * 
	 * @param indirizzo -> id indirizzo cliente
	 *
	 */
	Indirizzo delete(int idIndirizzo);
	
	/**
	 * Ricerca di tutti gli indirizzi cliente per via e comune
	 * 
	 * @param via ->  via indirizzo cliente
	 * @param comune -> comune indirizzo cliente
	 *
	 */
	List<Indirizzo> getByViaComune(String via, ComunePojoRicerca comune);
	
	/**
	 * Ricerca di tutti gli indirizzi cliente per localita
	 * 
	 * @param indirizzo ->  indirizzo indirizzo cliente
	 *
	 */
	List<Indirizzo> getByLocalita(Indirizzo indirizzo);
	
	/**
	 * Ricerca di tutti gli indirizzi cliente per comune
	 * 
	 * @param comune ->  comune indirizzo cliente
	 *
	 */
	List<Indirizzo> getByComune(ComunePojoRicerca comune);
	
	/**
	 * Ricerca di tutti gli indirizzi cliente del cliente
	 * 
	 * @param idCliente ->  indirizzi del cliente scelto
	 *
	 */
	List<Indirizzo> getByCliente(int idCliente);
	
	/**
	 * Ricerca di tutti gli indirizzi cliente per Paginazione
	 * 
	 * @param page -> grandezza della pagina
	 * @param size -> elementi contenuti nella pagina
	 * @param dir -> direzione di visualizzazione ("ASC" - "DESC")
	 * @param sort -> parametro della classe indirizzo cliente di visualizzazione
	 *
	 */
	Page<Indirizzo> getAll(Integer size, Integer page, String dir, String sort);

	
}
