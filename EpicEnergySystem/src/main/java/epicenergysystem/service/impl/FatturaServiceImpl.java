package epicenergysystem.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.model.Fattura;
import epicenergysystem.repository.ClienteRepository;
import epicenergysystem.repository.FatturaRepository;
import epicenergysystem.repository.StatoRepository;
import epicenergysystem.service.FatturaService;

@Service
public class FatturaServiceImpl implements FatturaService{

	@Autowired
	FatturaRepository fatturaRepo;
	
	@Autowired
	ClienteRepository clienteRepo;
	
	@Autowired
	StatoRepository statoRepo;
	
	@Autowired
	ChangeDate changeRepo;

	public Fattura changeDate(Fattura fattura) {
		
		var c = fattura.getCliente(); 
		
		c.setDataUltimoContatto(fattura.getDataInserimento());
	
		clienteRepo.save(c);
		
		c.setDataUltimoContatto(fattura.getDataInserimento());
		
		clienteRepo.save(c);
		
		return fattura;
		
	}
	
	@Override
	public Fattura save(Fattura fattura) {
		try {
			
			var f = fatturaRepo.save(fattura);
			
			return changeDate(f);
		} catch (Exception e) {
			throw new AppServiceException("Fattura non salvata");
		}
	} 

	@Override
	public Fattura edit(Fattura fattura) {
		try {
			var f = fatturaRepo.findById(fattura.getId()).get();
						
			if(fattura.getImporto()!=null) f.setImporto(fattura.getImporto());
			if(fattura.getAnno()!=null) f.setAnno(fattura.getAnno());
			if(fattura.getDataInserimento()!=null) f.setDataInserimento(fattura.getDataInserimento());
			if(fattura.getNumeroFattura()!=null) f.setNumeroFattura(fattura.getNumeroFattura());
			if(fattura.getStato()!=null) f.setStato(fattura.getStato());
			if(fattura.getCliente()!=null) f.setCliente(fattura.getCliente());
			
			return fatturaRepo.save(f);
		} catch (Exception e) {
			throw new AppServiceException("Fattura non modificata");
		}
	}

	@Override
	public Fattura delete(int idFattura) {
		try {
			var f = fatturaRepo.findById(idFattura).get();
			fatturaRepo.delete(f);
			return f;
		} catch (Exception e) {
			throw new AppServiceException("Fattura non eliminata per id: " + idFattura);
		}
	}

	@Override
	public List<Fattura> getByStato(String statoFattura) {
		try {
			var stato = statoRepo.findByStatoFattura(statoFattura);
			return fatturaRepo.findByStato(stato);
		} catch (Exception e) {
			throw new AppServiceException("Nessuna fattura trovata per lo stato: " + statoFattura);
		}
	}

	@Override
	public List<Fattura> getByDataInserimento(Fattura fattura) {
		try {
			return fatturaRepo.findByDataInserimento(fattura.getDataInserimento());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Fattura> getByRangeImporto(BigDecimal rangeStart, BigDecimal rangeEnd) {
		try {
			return fatturaRepo.findByRange(rangeStart, rangeEnd);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public List<Fattura> getByRangeImportoForCLiente(int idCliente, BigDecimal rangeStart, BigDecimal rangeEnd) {
		try {
			return fatturaRepo.findByRangeCliente(idCliente, rangeStart, rangeEnd);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	public List<Fattura> getByAnno(int idCliente, int anno) {
		try {			
			return fatturaRepo.getByAnno(idCliente, anno);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	
	
	@Override
	public Page<Fattura> getAll(Integer page, Integer size, String dir, String sort) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(dir), sort);
			return fatturaRepo.findAll(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
