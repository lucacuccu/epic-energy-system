package epicenergysystem.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.model.Cliente;
import epicenergysystem.repository.ClienteRepository;
import epicenergysystem.repository.ClienteRepository.ImportoPerAnno;
import epicenergysystem.repository.IndirizzoRepository.ClientePerProvincia;
import epicenergysystem.repository.ContattoClienteRepository;
import epicenergysystem.repository.FatturaRepository;
import epicenergysystem.repository.IndirizzoRepository;
import epicenergysystem.repository.TipoClienteRepository;
import epicenergysystem.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	ClienteRepository clienteRepo;
	
	@Autowired
	TipoClienteRepository tipoClienteRepo;
	
	@Autowired
	FatturaRepository fatturaRepo;
	
	@Autowired
	ContattoClienteRepository contattoRepo; 
	
	@Autowired
	IndirizzoRepository indirizzoRepo;
	
	@Override
	public Cliente save(Cliente cliente) {
		try {
			return clienteRepo.save(cliente);
		} catch (Exception e) {
			throw new AppServiceException("Cliente non salvato");
		}
	} 

	@Override
	public Cliente edit(Cliente cliente) {
		try {
			var c = clienteRepo.findById(cliente.getId()).get(); 
			
			if(cliente.getRagioneSociale()!=null) if(!cliente.getRagioneSociale().isBlank()) c.setRagioneSociale(cliente.getRagioneSociale());
			if(cliente.getPartitaIva()!=null) c.setPartitaIva(cliente.getPartitaIva());
			if(cliente.getEmail()!=null) if(!cliente.getEmail().isBlank()) c.setEmail(cliente.getEmail());
			if(cliente.getPec()!=null) if(!cliente.getEmail().isBlank()) c.setPec(cliente.getPec());
			if(cliente.getTelefono()!=null) if(!cliente.getTelefono().isBlank()) c.setTelefono(cliente.getTelefono());
			if(cliente.getTipoCliente()!=null) c.setTipoCliente(cliente.getTipoCliente());

			return clienteRepo.save(c);
		} catch (Exception e) {
			throw new AppServiceException("Cliente non salvato");
		}
		
	}

	@Override
	public Cliente delete(int cliente) {
		try {	 
			var c = clienteRepo.findById(cliente).get();
			
			fatturaRepo.findByCliente(c).stream().forEach(f -> fatturaRepo.delete(f));
			indirizzoRepo.findByCliente(c).stream().forEach(i -> indirizzoRepo.delete(i));
			contattoRepo.findByCliente(c).stream().forEach(x -> contattoRepo.delete(x));
						
			clienteRepo.delete(c); 
			return c;
		} catch (Exception e) {
			throw new AppServiceException("Cliente non eliminato");
		}
	}

	@Override
	public Cliente getById(int idCliente) {
		try {	
			return clienteRepo.findById(idCliente).get();
		} catch (Exception e) {
			throw new AppServiceException("Cliente non trovato per id: " + idCliente);
		}
	}

	@Override
	public Cliente getByEmail(String email) {
		try {	
			return clienteRepo.findByEmail(email);
		} catch (Exception e) {
			throw new AppServiceException("Cliente non trovato per questa email: " + email);
		}
	}

	@Override
	public Cliente getByPartitaIva(String partitaIva) {
		try {	
			return clienteRepo.findByPartitaIva(partitaIva);
		} catch (Exception e) {
			throw new AppServiceException("Cliente non trovato per questa Partita Iva: " + partitaIva);
		}
	}

	@Override
	public Cliente getByPec(String pec) {
		try {	
			return clienteRepo.findByPec(pec);
		} catch (Exception e) {
			throw new AppServiceException("Cliente non trovato per questa pec: " + pec);
		}
	}

	@Override
	public List<Cliente> getByTipoCliente(String tipoCliente) {
		try {	
			var tipo = tipoClienteRepo.findByStatoCliente(tipoCliente);
			return clienteRepo.findByTipoCliente(tipo);
		} catch (Exception e) {
			throw new AppServiceException("Cliente non trovato per questo id tipo cliente: " + tipoCliente);
		}
	}
	
	/* METODO getAllClienteBySort() -> cerca tutti i clienti tramite il parametro sort*/	
	@Override
	public Page<Cliente> getAllClienteBySort(Integer page, Integer size, String dir, String sort) {
		
		try {	
			Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
			return clienteRepo.findAll(pageable);
		} catch (Exception e) {
			throw new AppServiceException();
		}
		
	}
	
	@Override
	public List<ImportoPerAnno> getImportiByAnno(int anno) {
		try {
			return clienteRepo.getImportiByAnno(anno);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public BigDecimal getImportoByAnno(int idCliente, int anno) {
		try {			
			return clienteRepo.getImportoByAnno(idCliente, anno);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public List<Cliente> getByDataInserimentoOrder() {
		try {	
			return clienteRepo.findByDataInserimentoOrder();
		} catch (Exception e) {
			throw new AppServiceException("Cliente non trovato per questo il range di date inserito");
		}
	}
	
	@Override
	public Page<Cliente> findAllByOrderByDataUltimoContatto(Integer page, Integer size, String dir, String sort) {
		try {			
			Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(dir), sort);
			return clienteRepo.findAllByOrderByDataUltimoContatto(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public List<Cliente> getByRangeDataInserimento(Date start, Date end) {
		try {	
			return clienteRepo.findByDataInserimento(start, end);
		} catch (Exception e) {
			throw new AppServiceException("Cliente non trovato per questo il range di date inserito");
		} 
	}
	
	@Override
	public List<Cliente> getByDataUltimoContatto(Date date) {
		try {			 
			return clienteRepo.findByDataUltimoContattoContains(date);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public List<Cliente> getByAnnoUltimoContattoContains(int anno) {
		try {			 
			return clienteRepo.findByAnnoUltimoContattoContains(anno);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public List<Cliente> getByContainsRagioneSociale(String ragioneSociale){
		try {
			return clienteRepo.findByRagioneSocialeContains(ragioneSociale);
		} catch (Exception e) {
			throw new AppServiceException("Cliente non trovato per la ragione sociale inserita: " + ragioneSociale);
		}
	}
	
	@Override
	public List<ClientePerProvincia> getBySigla(String sigla) {
		try {
			return indirizzoRepo.findClienteBySiglaProvincia(sigla);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
