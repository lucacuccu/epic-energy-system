package epicenergysystem.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import epicenergysystem.security.model.Ruolo;
import epicenergysystem.security.repo.RuoloRepository;
import epicenergysystem.service.RuoloService;


@Service
public class RuoloServiceImpl implements RuoloService {
	
	@Autowired
	RuoloRepository roleRepository;

	@Override
	public Optional<Ruolo> findById(Long id) {
		return roleRepository.findById(id);
	}
	
	@Override
	public Ruolo save(Ruolo role) {
		return roleRepository.save(role);
	}
	
	@Override
	public Page<Ruolo> getAllRoleBySort(Integer page, Integer size, String sort) {
			
		Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
		Page<Ruolo> pagedResult = roleRepository.findAll(pageable);
		if(pagedResult.hasContent()) return pagedResult;
    		else return null;
		
	}
	
}
