package epicenergysystem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.security.model.Ruolo;
import epicenergysystem.security.model.Utente;
import epicenergysystem.security.repo.RuoloRepository;
import epicenergysystem.security.repo.UtenteRepository;
import epicenergysystem.service.UtenteService;

@Service
public class UtenteServiceImpl implements UtenteService {

	@Autowired
	UtenteRepository utenteRepo;
	
	@Autowired
	RuoloRepository roleRepo;
	
	@Autowired
	PasswordEncoder encoder;
	
	@Override
	public Utente findById(int id){
		try { 
			return utenteRepo.findById(id).get();
		} catch (Exception e) {
			throw new AppServiceException("Utente non trovato");
		} 
	}
	
	@Override
	public Page<Utente> getAllUtenteBySort(Integer page, Integer size, String dir, String sort) {
		
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(dir), sort);
			Page<Utente> pagedResult = utenteRepo.findAll(pageable);
			if(pagedResult.hasContent()) return pagedResult;
	    		else return null;
		} catch (Exception e) {
			throw new RuntimeException();
		}
		
	}
	
	@Override
	public Utente save(int idRuolo, Utente utente){

		String hashedPassword = encoder.encode(utente.getPassword());
		utente.setPassword(hashedPassword);
		var u = utenteRepo.save(utente); 
		
		u.getRoles().add(roleRepo.findById(idRuolo).get());
		
		return utenteRepo.save(u);
	}
	
	@Override
	public Utente delete(int utente){
		
		var u = utenteRepo.findById(utente).get();
		Ruolo a = roleRepo.findByRoleType("ROLE_ADMIN").get();
		Ruolo m = roleRepo.findByRoleType("ROLE_MAINTAINER").get();
		if(!u.getRoles().contains(a) || !u.getRoles().contains(m)){
			u.getRoles().clear();
			utenteRepo.delete(u);
		}
		
		return u;
	}
	
	@Override
	public Utente findByIdUsername(String username){
		return utenteRepo.findByUsername(username).get();
	}
	
	@Override
	public void criptAllPasswords(){
		
		List<Utente> list = utenteRepo.findAll();
		
		for(Utente u : list) {
			String hashedPassword = encoder.encode(u.getPassword());
			u.setPassword(hashedPassword);
			utenteRepo.save(u);
		}
		
	}
	
}