package epicenergysystem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.loader_file.repo.ComuneRepository;
import epicenergysystem.loader_file.repo.ProvinciaRepository;
import epicenergysystem.model.Cliente;
import epicenergysystem.model.Indirizzo;
import epicenergysystem.model.SedeIndirizzo;
import epicenergysystem.pojo_models.ComunePojoRicerca;
import epicenergysystem.repository.ClienteRepository;
import epicenergysystem.repository.IndirizzoRepository;
import epicenergysystem.repository.IndirizzoRepository.ClientePerProvincia;
import epicenergysystem.service.IndirizzoService;

@Service
public class IndirizzoServiceImpl implements IndirizzoService {
	
	@Autowired
	IndirizzoRepository indirizzoRepo;
	
	@Autowired
	ClienteRepository clienteRepo;
	
	@Autowired
	ComuneRepository comuneRepo;

	@Autowired
	ProvinciaRepository provinciaRepo;

	@Override
	public Indirizzo save(Indirizzo indirizzo) {
		try {	
			
			var c = indirizzo.getCliente();
			
			if(indirizzoRepo.findByClienteAndTipo(c, SedeIndirizzo.SEDE_LEGALE)!=null) {
				indirizzo.setTipo(SedeIndirizzo.SEDE_OPERATIVA);
			}
			if(indirizzoRepo.findByClienteAndTipo(c, SedeIndirizzo.SEDE_OPERATIVA)!=null) {
				throw new AppServiceException();
			}
			
			return indirizzoRepo.save(indirizzo);
			
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Indirizzo edit(Indirizzo indirizzo) {
		try {
			var i = indirizzoRepo.findByClienteAndTipo(indirizzo.getCliente(), indirizzo.getTipo());
			
			if(indirizzo.getTipo()!=null) i.setTipo(i.getTipo()); 
			if(indirizzo.getVia()!=null) if(!indirizzo.getVia().isBlank()) i.setVia(indirizzo.getVia()); 
			if(indirizzo.getCivico()!=null) if(!indirizzo.getCivico().isBlank()) i.setCivico(indirizzo.getCivico()); 
			if(indirizzo.getLocalita()!=null) if(!indirizzo.getLocalita().isBlank()) i.setLocalita(indirizzo.getLocalita()); 
			if(indirizzo.getCap()!=null) if(!indirizzo.getCap().isBlank()) i.setCap(indirizzo.getCap()); 
			if(indirizzo.getComune()!=null) i.setComune(indirizzo.getComune()); 
			if(indirizzo.getCliente()!=null) i.setCliente(i.getCliente());
			
			return indirizzoRepo.save(i);
		} catch (AppServiceException e) {
			throw new AppServiceException("Utente non modificato");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Indirizzo delete(int idIndirizzo) {
		try {
			var i = indirizzoRepo.findById(idIndirizzo).get();
			indirizzoRepo.delete(i);
			return i;
		} catch (Exception e) {
			throw new AppServiceException("Indirizzo non eliminato");
		}
	}
	
	@Override
	public List<Indirizzo> getByViaComune(String via, ComunePojoRicerca comune) {
		try {
			var p = provinciaRepo.findBySigla(comune.getSiglaProvincia()).get();
			var c = comuneRepo.findByNomeAndProvince(comune.getNomeComune(), p);
			return indirizzoRepo.findByViaAndComune(via, c);
		} catch (Exception e) {
			throw new AppServiceException();
		}
	}

	@Override
	public List<Indirizzo> getByLocalita(Indirizzo indirizzo) {
		try {
			return indirizzoRepo.findByLocalita(indirizzo.getLocalita());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Indirizzo> getByComune(ComunePojoRicerca comune) {
		try {
			var p = provinciaRepo.findBySigla(comune.getSiglaProvincia()).get();
			var c = comuneRepo.findByNomeAndProvince(comune.getNomeComune(), p);
			return indirizzoRepo.findByComune(c);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Indirizzo> getByCliente(int idCliente) {
		try {
			var c = clienteRepo.findById(idCliente).get();
			return indirizzoRepo.findByCliente(c);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Indirizzo> getAll(Integer size, Integer page, String dir, String sort) {
		try {
			Pageable pageable = PageRequest.of(page, size, Direction.valueOf(dir), sort);
			return indirizzoRepo.findAll(pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
