package epicenergysystem.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.model.basic.TipoCliente;
import epicenergysystem.repository.TipoClienteRepository;
import epicenergysystem.service.TipoClienteService;

@Service
public class TipoClienteServiceImpl implements TipoClienteService {

	@Autowired
	TipoClienteRepository tipoClienteRepo;
	
	@Override
	public TipoCliente save(TipoCliente tipoCliente) {
		try {
			return tipoClienteRepo.save(tipoCliente);
		} catch (Exception e) {
			throw new AppServiceException("Tipo cliente non salvato");
		}
	} 

	@Override
	public TipoCliente edit(TipoCliente tipoCliente) {
		try {
			var tipo = tipoClienteRepo.findById(tipoCliente.getId()).get();
			
			if(tipoCliente.getStatoCliente()!=null) 
				if(!tipoCliente.getStatoCliente().isBlank())
					tipo.setStatoCliente(tipoCliente.getStatoCliente());
			
			return tipoClienteRepo.save(tipo);
		} catch (Exception e) {
			throw new AppServiceException("Tipo cliente non modificato");
		}
	}

	@Override
	public TipoCliente delete(TipoCliente tipoCliente) {
		try {
			var tipo = tipoClienteRepo.findByStatoCliente(tipoCliente.getStatoCliente());
			tipoClienteRepo.delete(tipo);
			return tipo;
		} catch (Exception e) {
			throw new AppServiceException("Tipo cliente non eliminato");
		}
	}
	
	@Override
	public TipoCliente getByTipoCliente(TipoCliente tipoCliente) {
		try {
			return tipoClienteRepo.findByStatoCliente(tipoCliente.getStatoCliente());
		} catch (Exception e) {
			throw new AppServiceException("Tipo cliente non trovato per il nome inserito: " + tipoCliente.getStatoCliente());
		}
	}
	
	@Override
	public TipoCliente getByStatoCliente(String statoCliente) {
		try {
			 
			return tipoClienteRepo.findByStatoCliente(statoCliente);
		} catch (Exception e) {
			throw new AppServiceException("Tipo cliente non trovato per il nome inserito: " + statoCliente);
		}
	}
	
}