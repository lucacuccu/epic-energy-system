package epicenergysystem.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.model.ContattoCliente;
import epicenergysystem.repository.ClienteRepository;
import epicenergysystem.repository.ContattoClienteRepository;
import epicenergysystem.service.ContattoClienteService;

@Service
public class ContattoClienteServiceImpl implements ContattoClienteService{
	
	@Autowired
	ContattoClienteRepository contattoRepo;
	@Autowired
	ClienteRepository clienteRepo;

	@Override
	public ContattoCliente save(ContattoCliente contattoCliente) {
		try {
			return contattoRepo.save(contattoCliente); 
		} catch (Exception e) {
			throw new AppServiceException("Contatto cliente non salvato");
		}
	}

	@Override
	public ContattoCliente edit(ContattoCliente contattoCliente) {
		try {
			
			var c = contattoRepo.findByIdAndCliente(contattoCliente.getId(), contattoCliente.getCliente()); 
			
			if(contattoCliente.getNome()!=null) if(!contattoCliente.getNome().isBlank()) c.setNome(contattoCliente.getNome());
			if(contattoCliente.getCognome()!=null) if(!contattoCliente.getCognome().isBlank()) c.setCognome(contattoCliente.getCognome());
			if(contattoCliente.getEmail()!=null) if(!contattoCliente.getEmail().isBlank()) c.setEmail(contattoCliente.getEmail());
			if(contattoCliente.getTelefono()!=null) if(!contattoCliente.getTelefono().isBlank()) c.setTelefono(contattoCliente.getTelefono());
			c.setCliente(contattoCliente.getCliente());
			
			return contattoRepo.save(c); 
		} catch (Exception e) {
			throw new AppServiceException("Contatto cliente non modificato" + e);
		}
	} 

	@Override
	public ContattoCliente delete(int contattoCliente) {
		try {
			var c = contattoRepo.findById(contattoCliente).get(); 
			contattoRepo.delete(c);
			return c;
		} catch (Exception e) {
			throw new AppServiceException("Contatto cliente non eliminato");
		}
	}

	@Override
	public ContattoCliente getByEmail(String email) {
		try {
			return contattoRepo.findByEmail(email); 
		} catch (Exception e) {
			throw new AppServiceException("Contatto cliente non trovato per questa email: " + email);
		}
	}
	
	@Override
	public ContattoCliente getById(int id) {
		try {
			return contattoRepo.findById(id).get(); 
		} catch (Exception e) {
			throw new AppServiceException("Contatto cliente non trovato per questa email: " + id);
		}
	}

	@Override
	public List<ContattoCliente> getByCliente(int cliente) {
		try {
			var c = clienteRepo.findById(cliente).get();
			return contattoRepo.findByCliente(c);
		} catch (Exception e) {
			throw new AppServiceException("Contatti cliente non trovati per questo id cliente: " + cliente);
		}
	}

	@Override
	public Page<ContattoCliente> getAll(Integer page, Integer size, String dir, String sort) {
		try {	
			Pageable pageable = PageRequest.of(page, size, Sort.Direction.fromString(dir));
			return contattoRepo.findAll(pageable);
		} catch (Exception e) {
			throw new AppServiceException();
		}
	}

}
