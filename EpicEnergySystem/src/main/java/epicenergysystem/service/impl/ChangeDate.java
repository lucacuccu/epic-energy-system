package epicenergysystem.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epicenergysystem.model.Fattura;
import epicenergysystem.repository.ClienteRepository;
import epicenergysystem.repository.FatturaRepository;

@Service
public class ChangeDate {
	
	@Autowired
	ClienteRepository clienteRepo;
	
	@Autowired
	FatturaRepository fatturaRepo;
	
	public Fattura changeDate(Fattura fattura) {
		
		var c = fattura.getCliente();
		
		c.setDataUltimoContatto(fattura.getDataInserimento());
		
		clienteRepo.save(c);
		
		return fattura;
		
	}

}
