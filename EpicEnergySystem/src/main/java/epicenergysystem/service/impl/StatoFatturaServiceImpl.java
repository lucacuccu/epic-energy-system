package epicenergysystem.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import epicenergysystem.exceptions.AppServiceException;
import epicenergysystem.model.basic.StatoFattura;
import epicenergysystem.repository.StatoRepository;
import epicenergysystem.service.StatoFattutaService;

@Service
public class StatoFatturaServiceImpl implements StatoFattutaService {
	
	@Autowired
	StatoRepository statoRepo;

	@Override
	public StatoFattura save(StatoFattura stato) {
		try {
			return statoRepo.save(stato);
		} catch (Exception e) {
			throw new AppServiceException("Stato fattura non salvato");
		}
	}

	@Override
	public StatoFattura edit(StatoFattura stato) {
		try {
			
			var s = statoRepo.findById(stato.getId()).get();
			
			if(stato.getStatoFattura()!=null) if(!stato.getStatoFattura().isBlank()) s.setStatoFattura(stato.getStatoFattura());
			
			return statoRepo.save(s);
		} catch (Exception e) {
			throw new AppServiceException("Stato fattura non modificato");
		}
	}

	@Override
	public StatoFattura delete(String statoFattura) {
		try {	
			var s = statoRepo.findByStatoFattura(statoFattura);
			statoRepo.delete(s);
			return s;
		} catch (Exception e) {
			throw new AppServiceException("Stato fattura non eliminato");
		}
	}
	
	@Override
	public StatoFattura getByStatoFattura(StatoFattura stato) {
		try {
			return statoRepo.findByStatoFattura(stato.getStatoFattura());
		} catch (Exception e) {
			throw new AppServiceException();
		}
	}	
	
	@Override
	public StatoFattura getByStato(String stato) {
		try {
			return statoRepo.findByStatoFattura(stato);
		} catch (Exception e) {
			throw new AppServiceException();
		}
	}	
	
}
