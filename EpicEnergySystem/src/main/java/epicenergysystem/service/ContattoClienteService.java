package epicenergysystem.service;

import java.util.List;

import org.springframework.data.domain.Page;

import epicenergysystem.model.ContattoCliente;

/**
 * Gestione di un contatto cliente.
 * 
 * @author Luca Cuccu
 *
 */
public interface ContattoClienteService {
	
	/**
	 * Creazione di un contatto cliente
	 * 
	 * @param contattoCliente -> entita contatto cliente
	 *
	 */
	ContattoCliente save(ContattoCliente contattoCliente);
	
	/**
	 * Modifica di un contatto cliente
	 * 
	 * @param contattoCliente -> entita contatto cliente
	 *
	 */
	ContattoCliente edit(ContattoCliente contattoCliente);
	
	/**
	 * Eliminazione di un contatto cliente
	 * 
	 * @param contattoCliente -> id contatto cliente
	 *
	 */
	ContattoCliente delete(int contattoCliente);
	
	/**
	 * Ricerca di un contatto cliente per email
	 * 
	 * @param email -> email del contatto cliente
	 *
	 */
	ContattoCliente getByEmail(String contattoCliente);
	
	/**
	 * Ricerca di una lista di contatti cliente relativi ad un cliente specifico
	 * 
	 * @param cliente -> id del cliente
	 *
	 */
	List<ContattoCliente> getByCliente(int cliente);
	
	/**
	 * Ricerca di tutti i contatti cliente per Paginazione
	 * 
	 * @param page -> grandezza della pagina
	 * @param size -> elementi contenuti nella pagina
	 * @param dir -> direzione di visualizzazione ("ASC" - "DESC")
	 * @param sort -> parametro della classe contatto cliente di visualizzazione
	 *
	 */
	Page<ContattoCliente> getAll(Integer page, Integer size, String dir, String sort);
	
	/**
	 * Ricerca di un contatto cliente per id
	 * 
	 * @param id -> id del contatto cliente
	 *
	 */
	ContattoCliente getById(int id); 
	
}
