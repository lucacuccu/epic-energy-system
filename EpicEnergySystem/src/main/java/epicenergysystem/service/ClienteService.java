package epicenergysystem.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import epicenergysystem.model.Cliente;
import epicenergysystem.repository.ClienteRepository.ImportoPerAnno;
import epicenergysystem.repository.IndirizzoRepository.ClientePerProvincia;

/**
 * Gestione di un cliente.
 * 
 * @author Luca Cuccu
 *
 */
public interface ClienteService {

	/**
	 * Creazione di un cliente
	 * 
	 * @param cliente -> entita cliente
	 *
	 */
	Cliente save(Cliente cliente);
	
	/**
	 * Modifica di un cliente
	 * 
	 * @param cliente -> entita cliente
	 *
	 */
	Cliente edit(Cliente cliente);
	
	/**
	 * Eliminazione di un cliente
	 * 
	 * @param cliente -> id del cliente
	 *
	 */
	Cliente delete(int cliente);

	/**
	 * Ricerca di un cliente per id
	 * 
	 * @param idCliente -> id del cliente
	 *
	 */
	Cliente getById(int idCliente);
	
	/**
	 * Ricerca di un cliente per email
	 * 
	 * @param email -> email del cliente
	 *
	 */
	Cliente getByEmail(String email);
	
	/**
	 * Ricerca di un cliente per Partita Iva
	 * 
	 * @param partitaIva -> Partita Iva del cliente
	 *
	 */
	Cliente getByPartitaIva(String partitaIva);
	
	/**
	 * Ricerca di un cliente per Pec
	 * 
	 * @param pec -> Pec del cliente
	 *
	 */
	Cliente getByPec(String pec);
	
	/**
	 * Ricerca di un elenco di clienti per Tipo Cliente
	 * 
	 * @param tipoCliente -> Tipo del cliente
	 *
	 */
	List<Cliente> getByTipoCliente(String tipoCliente);
	
	/**
	 * Ricerca di tutti i clienti per Paginazione
	 * 
	 * @param page -> grandezza della pagina
	 * @param size -> elementi contenuti nella pagina
	 * @param dir -> direzione di visualizzazione ("ASC" - "DESC")
	 * @param sort -> parametro della classe cliente di visualizzazione
	 *
	 */
	Page<Cliente> getAllClienteBySort(Integer page, Integer size, String dir, String sort);
	
	/**
	 * Ricerca del fatturato annuale di un cliente
	 * 
	 * @param idCliente -> id del cliente
	 * @param anno -> anno scelto
	 *
	 */	
	BigDecimal getImportoByAnno(int idCliente, int anno);
	
	/**
	 * Ricerca del fatturato di tutti i clienti
	 * 
	 * @param anno -> anno scelto
	 *
	 */	
	List<ImportoPerAnno> getImportiByAnno(int anno);
	
	/**
	 * Ordinamento di tutti i clienti per data di inserimento
	 */	
	List<Cliente> getByDataInserimentoOrder();
	
	/**
	 * Ricerca di tutti i clienti per Paginazione ordinati per data Ultimo Contatto
	 * 
	 * @param page -> grandezza della pagina
	 * @param size -> elementi contenuti nella pagina
	 * @param dir -> direzione di visualizzazione ("ASC" - "DESC")
	 * @param sort -> parametro della classe cliente di visualizzazione
	 *
	 */
	Page<Cliente> findAllByOrderByDataUltimoContatto(Integer page, Integer size, String dir, String sort);

	/**
	 * Ricerca di una lista di clienti in un range di Data Inserimento
	 * 
	 * @param start -> data piu piccola
	 * @param end -> data piu grande
	 *
	 */	
	List<Cliente> getByRangeDataInserimento(Date start, Date end);
	
	/**
	 * Ricerca di una lista di clienti in un range di Data di Ultimo Contatto
	 * 
	 * @param date -> data ultimo contatto
	 *
	 */	
	List<Cliente> getByDataUltimoContatto(Date date);
	
	/**
	 * Ricerca di una lista di clienti in un range di date ultimo contatto di un anno specifico
	 * 
	 * @param anno -> anno specifico
	 *
	 */	
	List<Cliente> getByAnnoUltimoContattoContains(int anno);
	
	/**
	 * Ricerca di una lista di clienti contenenti parte di Ragione Sociale
	 * 
	 * @param ragioneSociale -> parte di Ragione Sociale
	 *
	 */	
	List<Cliente> getByContainsRagioneSociale(String ragioneSociale);

	/**
	 * Ricerca di una lista di clienti con relativo indirizzo (impostato tramite query di tipo SEDE_LEGALE) per provincia
	 * 
	 * @param sigla -> sigla provincia da cercare
	 *
	 */
	List<ClientePerProvincia> getBySigla(String sigla);
	
	
	
	
	
	
}
