package epicenergysystem.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import epicenergysystem.model.Cliente;
import epicenergysystem.model.basic.TipoCliente;
public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Integer>{
	
	public interface ImportoPerAnno {
        /**
         * @return il cliente
         */
        Cliente getCliente();
        /**
         * @return il fatturato
         */
        BigDecimal getImporto();
    }
	
	Optional<Cliente> findById(Integer id);
	
	Cliente findByEmail(String email);
	Cliente findByPartitaIva(String partitaIva);
	List<Cliente> findByTipoCliente(TipoCliente tipoCliente);
	Cliente findByPec(String pec);
	Page<Cliente> findAll(Pageable pageable);
	
	/* CLIENTI ORDINATI PER: */
	
	// Nome / Ragione Sociale
	@Query("SELECT c FROM Cliente c ORDER BY c.ragioneSociale")
	List<Cliente> findByRagioneSocialeOrder();
	
	/**
     * Fatturato annuo per tutti i clienti.
     * 
     * @param anno -> anno di riferimento.
    */
	@Query("SELECT (SELECT c FROM Cliente c WHERE f.cliente.id = c.id) as cliente, SUM(f.importo) AS importo FROM Fattura f WHERE f.anno=:anno GROUP BY f.cliente.id")
    List<ImportoPerAnno> getImportiByAnno(int anno);
	
	// Fatturato Annuale Azienda Specifica
	@Query("SELECT SUM(f.importo) FROM Fattura f WHERE f.cliente.id=:idCliente AND f.anno=:anno")
	BigDecimal getImportoByAnno(Integer idCliente, int anno);
	
	// Data di inserimento
	@Query("SELECT c FROM Cliente c ORDER BY c.dataInserimento")
	List<Cliente> findByDataInserimentoOrder();
	
	// Data ultimo contatto
	Page<Cliente> findAllByOrderByDataUltimoContatto(Pageable pageable);
	
	
	/* CLIENTI FILTRATI PER: */
	
	// Data di inserimento (con between)
	@Query("SELECT c FROM Cliente c WHERE c.dataInserimento BETWEEN :rangeStart AND :rangeEnd")
	List<Cliente> findByDataInserimento(@Param("rangeStart")Date rangeStart, @Param("rangeEnd")Date rangeEnd);
		
	// Data di ultimo contatto
	@Query("SELECT c FROM Cliente c WHERE c.dataUltimoContatto=:dataUltimoContatto")
	List<Cliente> findByDataUltimoContattoContains(Date dataUltimoContatto);
	
	// Data di ultimo contatto di quell'anno
	@Query("SELECT c FROM Cliente c WHERE YEAR(c.dataUltimoContatto) =:annoData")
	List<Cliente> findByAnnoUltimoContattoContains(int annoData);
	
	// Parte del nome
	List<Cliente> findByRagioneSocialeContains(String ragioneSociale);

}
