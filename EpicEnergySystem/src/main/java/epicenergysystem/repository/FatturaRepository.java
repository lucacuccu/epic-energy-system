package epicenergysystem.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import epicenergysystem.model.Cliente;
import epicenergysystem.model.Fattura;
import epicenergysystem.model.basic.StatoFattura;

public interface FatturaRepository extends PagingAndSortingRepository<Fattura, Integer> {

	@Query("SELECT f FROM Fattura f")
	Page<Fattura> findAll(Pageable pageable);
	
	List<Fattura> findByCliente(Cliente cliente);
	
	List<Fattura> findByStato(StatoFattura stato);
	
	Fattura findByNumeroFatturaAndCliente(Integer numeroFattura, Cliente idCliente);
	
	List<Fattura> findByDataInserimento(Date dataInserimento);
	
	@Query("SELECT f FROM Fattura f WHERE f.cliente.id=:idCliente AND f.anno=:anno")
	List<Fattura> getByAnno(int idCliente, int anno);

	@Query("SELECT f FROM Fattura f WHERE f.importo BETWEEN :rangeMin AND :rangeMax")
	List<Fattura> findByRange(@Param("rangeMin") BigDecimal rangeMin, @Param("rangeMax") BigDecimal rangeMax);
	
	@Query("SELECT f FROM Fattura f WHERE f.cliente.id =:idCliente AND f.importo BETWEEN :rangeMin AND :rangeMax ")
	List<Fattura> findByRangeCliente(int idCliente, @Param("rangeMin") BigDecimal rangeMin, @Param("rangeMax") BigDecimal rangeMax);

}
