package epicenergysystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import epicenergysystem.model.basic.StatoFattura;

public interface StatoRepository extends JpaRepository<StatoFattura, Integer> {

	StatoFattura findByStatoFattura(String statoFattura);
	
}
