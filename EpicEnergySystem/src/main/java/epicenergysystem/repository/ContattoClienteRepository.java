package epicenergysystem.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import epicenergysystem.model.Cliente;
import epicenergysystem.model.ContattoCliente;

public interface ContattoClienteRepository extends JpaRepository<ContattoCliente, Integer> {

	ContattoCliente findByEmail(String email);
	List<ContattoCliente> findByCliente(Cliente cliente);
	ContattoCliente findByIdAndCliente(int id, Cliente cliente);
	Page<ContattoCliente> findAll(Pageable pageable);
	
}
