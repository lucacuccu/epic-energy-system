package epicenergysystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import epicenergysystem.model.basic.TipoCliente;

public interface TipoClienteRepository extends JpaRepository<TipoCliente, Integer> {

	TipoCliente findByStatoCliente(String statoCliente);
	
}
