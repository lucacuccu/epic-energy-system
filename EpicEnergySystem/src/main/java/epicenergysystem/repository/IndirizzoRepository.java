package epicenergysystem.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import epicenergysystem.model.Cliente;
import epicenergysystem.model.Indirizzo;
import epicenergysystem.model.SedeIndirizzo;
import epicenergysystem.model.entityfromfile.Comune;

public interface IndirizzoRepository extends JpaRepository<Indirizzo, Integer> {
	
	List<Indirizzo> findByViaAndComune(String via, Comune comune);
	List<Indirizzo> findByCivico(String civico);
	List<Indirizzo> findByLocalita(String localita);
	List<Indirizzo> findByCap(String cap);
	List<Indirizzo> findByComune(Comune comune);
	List<Indirizzo> findByCliente(Cliente cliente);
	
	Indirizzo findByClienteAndTipo(Cliente cliente, SedeIndirizzo tipo);
	
	public interface ClientePerProvincia {
        /**
         * @return il cliente
         */
        Cliente getCliente();
        /**
         * @return l'indirizzo
         */
        Indirizzo getIndirizzo();
    }
	
	@Query("SELECT c as cliente, i as indirizzo FROM Cliente c, Indirizzo i, Comune cc, Provincia p WHERE i.cliente.id=c.id AND i.tipo='SEDE_LEGALE' AND i.comune.id=cc.id AND cc.province.id=p.id and p.sigla=:sigla")
	List<ClientePerProvincia> findClienteBySiglaProvincia(String sigla);
	
	Page<Indirizzo> findAll(Pageable pageable);

}
